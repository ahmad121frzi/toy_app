class User < ActiveRecord::Base
   has_many :microposts
   
  validates :name, length: {minimum: 1}
  validates :gender.downcase, format:   { with:  /male/}
  validates :gender.downcase, format:   { with:  /female/}
  validates :email, length: {minimum: 1}
  
end
